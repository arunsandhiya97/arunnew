package com.leafBot.testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.leafBot.pages.LoginPage;
import com.leafBot.pages.MyHomePage;
import com.leafBot.testng.api.base.Annotations;




public class TC001_CreateLead extends Annotations {
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_LoginAndLogout";
		testcaseDec = "Login into leaftaps";
		author = "koushik";
		category = "sanity";
		excelFileName = "TC001";
	} 

	
	@Test(dataProvider="fetchData")
	public void createLeadTest(String cName, String fName, String lName) {
		new LoginPage().enterUserName("Demosalesmanager").enterPassWord("crmsfa").clickLogin()
		.clickLeadsTab()
		.clickCreateLead()
		.typeCompanyName(cName)
		.typeFirstName(fName)
		.typeLastName(lName)
		.clickCreateLeadButton()
		.verifyFirstName(fName);
	}
	
	
	
	
	
	
	
	
	

}
