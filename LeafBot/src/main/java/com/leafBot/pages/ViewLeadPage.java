package com.leafBot.pages;

import com.leafBot.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations {
	
	public ViewLeadPage verifyFirstName(String s) {
//		String text = driver.findElementById("viewLead_firstName_sp")
//		.getText();
//		if(text.equals(s)) {
//			System.out.println("First name matches with input data");
//		}else {
//			System.err.println("First name not matches with input data");
//		}
//		return this;
		String elementText = getElementText(driver.findElementById("viewLead_firstName_sp"));
		if(elementText.equals(s)) {
			System.out.println("First name matches with input data");
		}else {
			System.err.println("First name not matches with input data");
		}
		return this;
	}
	
	public ViewLeadPage deleteRecord() {
//		driver.findElementByXPath("//a[@href='javascript:document.deleteLeadForm.submit()']").click();
		click(driver.findElementByXPath("//a[@href='javascript:document.deleteLeadForm.submit()']"));
		return this;
		
		
	}
	public String getleadID() {
//		String text=driver.findElementById("viewLead_companyName_sp").getText();
//		String lead=text.replaceAll("\\D", "");
//		return lead;
		String elementText = getElementText(driver.findElementById("viewLead_companyName_sp"));
		String lead=elementText.replaceAll("\\D", "");
		return lead;
		
	}

	
	
	
	
	
	
}
