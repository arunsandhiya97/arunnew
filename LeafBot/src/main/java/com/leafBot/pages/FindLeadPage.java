package com.leafBot.pages;

import com.leafBot.testng.api.base.Annotations;

public class FindLeadPage extends Annotations {
	public FindLeadPage clickFindLeadPage() throws InterruptedException {
//		driver.findElementByXPath("//a[@href='/crmsfa/control/findLeads']").click();
//		Thread.sleep(2000);
//		return this;
		click(driver.findElementByXPath("//a[@href='/crmsfa/control/findLeads']"));
		Thread.sleep(2000);
		return this;
	}
	public FindLeadPage typeFirstName(String s) {
//		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(s);
//		return this;
		clearAndType(driver.findElementByXPath("(//input[@name='firstName'])[3]"), s);
		return this;
	}
	public FindLeadPage clickFindLeads() throws InterruptedException {
//		driver.findElementByXPath("//button[text()='Find Leads']").click();
//		Thread.sleep(3000);
//		return this;
		click(driver.findElementByXPath("//button[text()='Find Leads']"));
		
		Thread.sleep(3000);
		return this;
	}
	public ViewLeadPage clickRecord() {
//		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
//		return new ViewLeadPage();
		click(driver.findElementByXPath("(//a[@class='linktext'])[4]"));
		return new ViewLeadPage();
	}
	public FindLeadPage typeLeadID(String lid) {
//		driver.findElementByXPath("(//label[text()='Lead ID:']/following::input)[1]").sendKeys(lid);
//		return this;
		clearAndType(driver.findElementByXPath("(//label[text()='Lead ID:']/following::input)[1]"), lid);
		return this;
	
	}
	
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


